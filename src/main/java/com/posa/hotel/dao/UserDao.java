package com.posa.hotel.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Repository
public class UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public User getUserByUsername(String username) {

        List<User> userList = jdbcTemplate.query("select * from clienti where numeutilizator = '" + username + "';", new RowMapper<User>() {
            @Nullable
            @Override
            public User mapRow(ResultSet resultSet, int i) throws SQLException {
                User user = new User();
                user.setFirstName(resultSet.getString("prenume"));
                user.setLastName(resultSet.getString("nume"));
                user.setUsername(resultSet.getString("numeutilizator"));
                user.setPassword(resultSet.getString("parola"));
                user.setId(resultSet.getInt("id"));

                return user;
            }
        });

        if (userList.size() > 0) {
            return userList.get(0);
        }
        return null;
    }
}
