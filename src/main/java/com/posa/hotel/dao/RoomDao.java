package com.posa.hotel.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public class RoomDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void saveBooking(LocalDate dateStart, LocalDate dateEnd, Integer roomId, Integer userId) {
        jdbcTemplate.update("insert into rezervari values(null, ?, ?, ?, ?)", roomId, dateStart, dateEnd, userId);
    }

    public List<Room> getAllRooms() {
        return jdbcTemplate.query("select * from camere", new RowMapper<Room>() {

            @Nullable
            @Override
            public Room mapRow(ResultSet resultSet, int i) throws SQLException {
                Room room = new Room();
                room.setDescription(resultSet.getString("descriere"));
                room.setTitle(resultSet.getString("titlu"));
                room.setPrice(resultSet.getBigDecimal("pret"));
                room.setId(resultSet.getInt("id"));

                return room;
            }
        });
    }
}
