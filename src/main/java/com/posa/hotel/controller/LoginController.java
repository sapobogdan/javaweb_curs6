package com.posa.hotel.controller;

import com.posa.hotel.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/login")
    public String login(@RequestParam("username") String username, @RequestParam("password") String password) {
        boolean loginResult = loginService.loginUser(username, password);
        if(loginResult) {
            return "redirect:/rooms";
        }

        return "redirect:/login.html";
    }
}
