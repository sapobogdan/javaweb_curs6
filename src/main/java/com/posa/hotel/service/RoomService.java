package com.posa.hotel.service;

import com.posa.hotel.dao.Room;
import com.posa.hotel.dao.RoomDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class RoomService {

    @Autowired
    private RoomDao roomDao;

    public List<Room> getAllRooms() {
        return roomDao.getAllRooms();
    }

    public void bookRoom(LocalDate dateStart, LocalDate dateEnd, Integer roomId, Integer userId) {
        roomDao.saveBooking(dateStart, dateEnd, roomId, userId);
    }

}
